var container;
var expandedContent;
var collapsedContent;
var expandButton;
var closeButton;
var closeAdButton;
// var video;
var creativeBackground;
var creativeBackgroundDimensions;

function checkIfEBInitialized(e) {
  if (EB.isInitialized()) {
    initializeCreative();
  }
  else {
    EB.addEventListener(EBG.EventName.EB_INITIALIZED, initializeCreative);
  }
}

function initializeCreative(e) {
  initializeGlobalVariables();
  addEventListeners();
  // trackVideoInteractions(video);
  EB._sendMessage('additionalConfig', {fullwidthBanner: false});
}

function initializeGlobalVariables() {
  container = document.getElementById("container");
  expandedContent = document.getElementById("expanded-content");
  collapsedContent = document.getElementById("collapsed-content");
  expandButton = document.getElementById("expand-button");
  closeAdButton = document.getElementById("close-ad-button");
  closeButton = document.getElementById("close-button");
  // video = document.getElementById("video");
  creativeBackground = document.getElementById("creative-background");
  creativeBackgroundDimensions = document.getElementById("creative-background-dimensions");
}

function addEventListeners() {
  // var userActionButton = document.getElementById("user-action-button");
  var clickthroughButton = document.getElementById("clickthrough-button");

  // userActionButton.addEventListener("click", trackUserAction);
  clickthroughButton.addEventListener("click", clickthrough);
  expandButton.addEventListener("mouseover", expand);
  closeButton.addEventListener("click", collapse);
  closeAdButton.addEventListener("click", closeAd);  
}

function trackUserAction(e) {
  EB.userActionCounter("UserAction");
}

function clickthrough(e) {
  EB.clickthrough();
}

function expand(e) {
  EB.expand();
  container.className = "expanded";
  collapsedContent.style.display = "none";
  expandedContent.style.display = "block";
  preventPageScrolling();
}

function preventPageScrolling() {
  document.addEventListener("touchmove", stopScrolling);
}

function stopScrolling(event) {
  event.preventDefault();
}

function collapse(e) {
  EB.collapse();
  container.className = "collapsed";
  expandedContent.style.display = "none";
  collapsedContent.style.display = "block";
  // video.pause();
  allowPageScrolling();
}

function allowPageScrolling() {
  document.removeEventListener("touchmove", stopScrolling);
}

function closeAd(e) {
  EB.userActionCounter("closeAd");
  EB._sendMessage("closeAd", {});
}

function trackVideoInteractions(video) {
  var videoTrackingModule = new EBG.VideoModule(video);
}

window.addEventListener("DOMContentLoaded", function(event){
	checkIfEBInitialized();
	if(navigator.userAgent.toLowerCase().indexOf("android 2") != -1){
		EB.currentWidth = window.innerWidth;
		EB.currentHeight = window.innerHeight;
		setInterval(function(){
			if(EB.currentWidth != window.innerWidth || EB.currentHeight != window.innerHeight){
				EB.currentWidth = window.innerWidth;
				EB.currentHeight = window.innerHeight;
				document.body.style.opacity = 0.99;
				setTimeout(function(){
					document.body.style.width = window.innerWidth;
					document.body.style.height = window.innerHeight;
					document.body.style.opacity = 1;
				}, 100);
			}
		}, 100);
	}
}, false);