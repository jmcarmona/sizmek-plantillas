/*******************
VARIABLES
*******************/
// var userActionButton;
var clickthroughButton;
var closeButton;
var videoContainer;
// var video;
var creativeBackground;
var creativeBackgroundDimensions;
var isiOS = (/ipod|iphone|ipad/i).test(navigator.userAgent);
var isWindowsPhone = (/windows phone/i).test(navigator.userAgent);

/*******************
INITIALIZATION
*******************/
function checkIfEBInitialized(event)
{
	if(!EB.isInitialized())
	{
		EB.addEventListener(EBG.EventName.EB_INITIALIZED, initializeCreative);
	}
	else
	{
		initializeCreative();
	}
}

function initializeCreative()
{
	initializeGlobalVariables();
	addEventListeners();
	// initializeVideo();
}

function initializeGlobalVariables()
{
	// userActionButton = document.getElementById("user-action-button");
	clickthroughButton = document.getElementById("clickthrough-button");
	closeButton = document.getElementById("close-button");
	videoContainer = document.getElementById("video-container");
	// video = document.getElementById("video");
	creativeBackground = document.getElementById("creative-background");
	creativeBackgroundDimensions = document.getElementById("creative-background-dimensions");
}

function initializeVideo()
{
	var sdkData = EB.getSDKData();
	var useSDKVideoPlayer = false;
	var sdkPlayerVideoFormat = "mp4"; // normally, App plays mp4 format, use "webm" if App plays webm format
	if (sdkData !== null)
	{
		if (sdkData.SDKType !== "MRAID" && sdkData.version > 1)
		{
			var sourceTags = video.getElementsByTagName("source");
			var videoSource = "";
			for (var i = 0 ; i < sourceTags.length; i ++)
			{
				if (sourceTags[i].getAttribute("type"))
				{
					if (sourceTags[i].getAttribute("type").toLowerCase() === "video/" + sdkPlayerVideoFormat)
					{
						videoSource = sourceTags[i].getAttribute("src");
					}
				}
			}
			videoContainer.removeChild(video);
			video = null;
			sdkVideoPlayButton.addEventListener("click", function()
			{
				if (videoSource !== "")
				{
					EB.playVideoOnNativePlayer(videoSource);
				}
			});
			useSDKVideoPlayer = true;
		}
	}
	if (!useSDKVideoPlayer)
	{
		videoContainer.removeChild(sdkVideoPlayer);
		var videoTrackingModule = new EBG.VideoModule(video);
		if (isWindowsPhone)
		{
			video.addEventListener("click", function(event)
			{
				var videoWidth = getStyle(this, "width");
				var videoHeight = getStyle(this, "height");
				if(videoWidth < 168 || videoHeight < 152)
				{
					video.play();
				}
			}, false);			
		}			
	}
	videoContainer.style.display = "block";
}

function addEventListeners()
{
	// userActionButton.addEventListener("click", trackUserAction);
	clickthroughButton.addEventListener("click", clickthrough);
	closeButton.addEventListener("click", collapse); 
	// if (isiOS)
	// {
	// 	video.addEventListener("webkitendfullscreen", handleEndFullScreen, false);
	// }	
}

/*******************
EVENT HANDLERS
*******************/
function trackUserAction()
{
	EB.userActionCounter("UserAction");
}

function clickthrough()
{
	// pauseVideo();
	EB.clickthrough();
}

function collapse() {
	// pauseVideo();
	EB.expand({panelName: (EB._adConfig && EB._adConfig.hasOwnProperty("mdBannerPanelName") ? EB._adConfig.mdBannerPanelName : "banner")});
}

function handleEndFullScreen()
{
	EB._sendMessage("endFullScreen", {});
}

/*******************
UTILITIES
*******************/
function pauseVideo()
{
	if (video)
	{
		video.pause();
	}
}

function getStyle(obj, styleName)
{
	try
	{
		if (typeof document.defaultView !== "undefined" && typeof document.defaultView.getComputedStyle !== "undefined")
		{
			return document.defaultView.getComputedStyle(obj, "")[styleName];
		}
		else if (typeof obj.currentStyle !== "undefined")
		{
			return obj.currentStyle[styleName];
		}
		else
		{
			return obj.style[styleName];
		}
		return null;
	}
	catch (error)
	{
		return null;
	}
}

window.addEventListener("DOMContentLoaded", checkIfEBInitialized, false);