/*******************
VARIABLES
*******************/
var expandButton;
var closeAdButton;

/*******************
INITIALIZATION
*******************/
function checkIfEBInitialized(event)
{
	if(!EB.isInitialized())
	{
		EB.addEventListener(EBG.EventName.EB_INITIALIZED, initializeCreative);
	}
	else
	{
		initializeCreative();
	}
}

function initializeCreative()
{
	initializeGlobalVariables();
	addEventListeners();
}

function initializeGlobalVariables()
{
	expandButton = document.getElementById("expand-button");
	closeAdButton = document.getElementById("close-ad-button");
}

function addEventListeners()
{
	expandButton.addEventListener("mouseover", expand);
	closeAdButton.addEventListener("click", closeAd);
}

/*******************
EVENT HANDLERS
*******************/
function expand()
{
	EB.expand({panelName: (EB._adConfig && EB._adConfig.hasOwnProperty("mdExpandPanelName") ? EB._adConfig.mdExpandPanelName : "expand")});
}

function closeAd()
{
	EB.userActionCounter("closeAd");
	EB._sendMessage("closeAd", {});
}

window.addEventListener("DOMContentLoaded", checkIfEBInitialized, false);